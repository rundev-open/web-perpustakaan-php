create database crud_db;
 
use crud_db;
 
CREATE TABLE `books` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(100),
  `category_id` integer(15),
  `author` varchar(15),
  `pages_count` varchar(15),
  PRIMARY KEY  (`id`)
);