<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Perpustakaan</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
      <a class="navbar-brand" href="#">Perpustakaan</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
          <a class="nav-link active" aria-current="page" href="index.php">Home</a>
          <a class="nav-link" href="daftarBuku.php">Daftar Buku</a>
          <a class="nav-link" href="kategori.php">Kategori</a>
          <a class="nav-link" href="pengurus.php">Pengurus</a>
        </div>
      </div>
      <form class="d-flex" role="search">
        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-light" type="submit">Search</button>
      </form>
    </div>
  </nav>
  <div class="w-100" style="height: 100vh; position: absolute;z-index: -10;">
    <img src="assets/images/1.jpg" alt="banner" style="width: inherit; height: inherit; object-fit: cover;">
  </div>
  <div class="container">
    <div class="bg-white p-3 m-3" style="border-radius: 20px;">
      <div class="h3">Daftar Buku</div>
      <a href="daftarBuku/create.php" class="btn btn-success">Tambah</a>
      <table class="table table-responsive">
        <thead>
          <tr>
            <th>#</th>
            <th>Judul Buku</th>
            <th>Kategori</th>
            <th>Pengarang</th>
            <th>Jumlah Halaman</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Angkara Sana</td>
            <td>Indah Purnama</td>
            <td>Misteri</td>
            <td>100</td>
            <td>
              <a href="daftarBuku/edit.php" class="btn btn-sm btn-warning">Edit</a>
              <a href="#" class="btn btn-sm btn-danger" onclick="confirm('Hapus ?')">Hapus</a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</body>

</html>