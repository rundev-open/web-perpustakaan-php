<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Perpustakaan</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
      <a class="navbar-brand" href="#">Perpustakaan</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
          <a class="nav-link active" aria-current="page" href="index.php">Home</a>
          <a class="nav-link" href="daftarBuku.php">Daftar Buku</a>
          <a class="nav-link" href="kategori.php">Kategori</a>
          <a class="nav-link" href="pengurus.php">Pengurus</a>
        </div>
      </div>
      <form class="d-flex" role="search">
        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-light" type="submit">Search</button>
      </form>
    </div>
  </nav>
  <div class="w-100" style="height: 100vh; position: absolute;z-index: -10;">
    <img src="../assets/images/1.jpg" alt="banner" style="width: inherit; height: inherit; object-fit: cover;">
  </div>
  <div class="container">
    <div class="bg-white p-3 m-3" style="border-radius: 20px;">
      <a href="../daftarBuku.php" class="btn btn-outline-danger my-1">Kembali</a>
      <h3>Tambah Daftar Buku</h3>
      <form action="#">
        <div class="row">
          <div class="col-6">
            <div class="form-group">
              <label for="" class="control-label">Judul Buku</label>
              <input type="text" class="form-control">
            </div>
          </div>
          <div class="col-6">
            <div class="form-group">
              <label for="" class="control-label">Pengarang</label>
              <input type="text" class="form-control">
            </div>
          </div>
          <div class="col-6">
            <div class="form-group">
              <label for="" class="control-label">Jumlah Halaman</label>
              <input type="text" class="form-control">
            </div>
          </div>
          <div class="col-6">
            <div class="form-group">
              <label for="" class="control-label">Kategori</label>
              <select name="" id="" class="form-select">
                <option value="">Komedi</option>
                <option value="">Horor</option>
                <option value="">Romantis</option>
                <option value="">Fantasi</option>
                <option value="">Misteri</option>
              </select>
            </div>
          </div>
          <div class="col-12 my-3">
            <button type="submit" class="btn btn-success">Simpan</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</body>

</html>